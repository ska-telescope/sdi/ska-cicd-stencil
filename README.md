# SKA CI/CD STENCIL

STENCIL Tests Enabling Normalized Continuous Integration Libraries

This repository is used for testing SKA CI/CD templates and Makefiles with the standardized repository structure with different artifacts.

The repository structure can be found in [Standardizing Project Structure and Content Confluence Page](https://confluence.skatelescope.org/display/SE/Standardising+Project+Structure+and+Content).

The templates repository and documentation about it could be found in [templates-repository](https://gitlab.com/ska-telescope/templates-repository).

The make targets and their documentation could be found in [ska-cicd-makefile repository](https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile).

## Testing under a specific `.make` submodule commit
If needed, the STENCIL pipeline can be run against a specific commit of the `.make` submodule. This is useful for testing changes to the `.make` submodule before they are merged into the master branch.

In order to do so, the variable `UPSTREAM_MAKEFILE_COMMIT_SHA` must be set to the desired commit SHA. This can be done from the GitLab UI by navigating to `Pipelines > Run Pipeline > Variables` and adding a new variable with the name `UPSTREAM_MAKEFILE_COMMIT_SHA` and the desired commit SHA as the value.
