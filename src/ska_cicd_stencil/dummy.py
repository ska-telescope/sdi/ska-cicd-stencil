"""
This module includes dummy function for the repository.

It is used for dummy testing python cicd templates
"""
import sys

import numba
import numpy as np

# from numba import cuda


def dummy_function():
    """A Dummy function

    Returns:
        str: dummy-function
    """
    return "dummy-function"


def cuda_dummy_function():
    """A CUDA instalation function

    Returns:
        str: cuda-function
    """
    print("Python version:", sys.version)
    print("Numba version:", numba.__version__)
    print("Numpy version:", np.__version__)
    return "cuda-function"


# can't run right now with CUDA due to lack of libraries on pytango images
# @cuda.jit
def cuda_kernel_function(array):
    """A CUDA function adding 1 to each element of the array

    Returns:
        np.array: array with 1 added to each element
    """
    for i in range(array.size):
        array[i] += 0.5

    return array
