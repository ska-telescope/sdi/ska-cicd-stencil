"""
This module includes test for the repository.

It works with pytest
"""
import numpy as np
import pytest

from ska_cicd_stencil import dummy


@pytest.mark.gputest
def test_cuda():
    """A dummy test for a cuda function"""
    test = dummy.cuda_dummy_function()
    assert test == "cuda-function"


@pytest.mark.gputest
def test_cuda_kernel_function():
    """Tests the cuda kernel function adding 0.5 to the input"""
    array = np.array([0, 1], np.float32)
    # can't run right now with CUDA due to lack of libraries on pytango images
    # test = dummy.cuda_kernel_function[1, 1](array)
    test = dummy.cuda_kernel_function(array)
    assert (test == [0.5, 1.5]).all()


def test_dummy():
    """A dummy test"""
    test = dummy.dummy_function()
    assert test == "dummy-function"
