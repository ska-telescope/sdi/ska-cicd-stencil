# Pull and switch to the triggering makefile commit or the latest on remote
# Note: variables are not used, we are only interested in to run the commands for every make call
ifneq ($(UPSTREAM_MAKEFILE_COMMIT_SHA),)
UPSTREAM_COMMIT_SET := $(shell echo "UPSTREAM_MAKEFILE_COMMIT_SHA is set to $(UPSTREAM_MAKEFILE_COMMIT_SHA)")
$(info $(UPSTREAM_COMMIT_SET))
IGNORE_PULL_MAKEFILE_TRIGGER := $(shell cd .make && git fetch origin +refs/heads/*:refs/remotes/origin/* && git fetch --all && git branch -r && git checkout $(UPSTREAM_MAKEFILE_COMMIT_SHA))
$(info $(IGNORE_PULL_MAKEFILE_TRIGGER))
else
UPSTREAM_COMMIT_SET := $(shell echo "UPSTREAM_MAKEFILE_COMMIT_SHA is not set. Using master branch.")
$(info $(UPSTREAM_COMMIT_SET))
IGNORE_PULL_MAKEFILE_MASTER := $(shell cd .make && git fetch origin +refs/heads/*:refs/remotes/origin/* && git fetch --all && git branch -r && git checkout master)
$(info $(IGNORE_PULL_MAKEFILE_MASTER))
endif

-include .make/base.mk
-include .make/ansible.mk
-include .make/bats.mk
-include .make/conan.mk
-include .make/cpp.mk
-include .make/helm.mk
-include .make/oci.mk
-include .make/python.mk
-include .make/raw.mk
-include .make/rpm.mk
-include .make/k8s.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

# Ansible collections path
ANSIBLE_COLLECTIONS=collections/ansible_collections

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_cicd_stencil

ifneq ($(strip $(CI_JOB_ID)),)
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/sdi/ska-cicd-stencil/ska-cicd-stencil:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
endif

oci-post-build-all: 
	printf "git says you have the following outstanding changes:\n $$(git status -s) \n";
