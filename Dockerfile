FROM artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.5.2


WORKDIR /app

# Install Poetry, Note: pip method shouldn't be used for local installations,
# It's good enough for pipelines as any conflict with system packages are not expected here
RUN pip install --no-cache-dir poetry==1.1.13

# Copy poetry.lock* in case it doesn't exist in the repo
COPY . /app/

# Install runtime dependencies and the app
RUN poetry install --no-dev

